import 'bootstrap/dist/css/bootstrap.min.css';

import { Container } from 'reactstrap';

import TitleComponent from "./components/title/TitleComponent";
import ContentComponent from "./components/content/ContentComponent";

function App() {
  return (
    <Container className='text-center'>
      <TitleComponent />
      <ContentComponent />
    </Container>
  );
}

export default App;
