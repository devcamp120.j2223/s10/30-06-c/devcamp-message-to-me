import { useState } from "react";

import InputMessage from "./input/InputMessage";
import OutputMessage from "./output/OutputMessage";

function ContentComponent() {
    const [messageInput, setMessageInput] = useState("Xin mời nhập message ...");
    const [messageOutput, setMessageOutput] = useState([]);
    const [likeImage, setLikeImage] = useState(false);

    const changeInputMessageHandler = (value) => {
        console.log(value);
        setMessageInput(value);
    }

    const changeOutputMessageHandler = () => {
        setMessageOutput([...messageOutput, messageInput]);
        setLikeImage(messageInput ? true : false);
    }

    return (
        <div>
            <InputMessage messageInputProp={messageInput} changeInputMessageProp={changeInputMessageHandler} changeOutputMessageProp={changeOutputMessageHandler} />
            <OutputMessage messageOutputProp={messageOutput} likeImageProp={likeImage} />
        </div>
    )
}

export default ContentComponent;