import { Row, Col, Label, Input, Button } from "reactstrap";

function InputMessage({messageInputProp, changeInputMessageProp, changeOutputMessageProp}) {

    const inputChangeHandler = (event) => {
        console.log("Giá trị input thay đổi")
        console.log(event.target.value)
        changeInputMessageProp(event.target.value)
    }

    const buttonClickHandler = () => {
        console.log("Nút được bấm")
        changeOutputMessageProp();
    }

    return (
        <div>
            <Row className="mt-3">
                <Col>
                    <Label>Message cho bạn 12 tháng tới:</Label>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Input placeholder={messageInputProp} onChange={inputChangeHandler}></Input>
                </Col>
            </Row>
            <Row className="mt-3">
                <Col>
                    <Button color="success" onClick={buttonClickHandler}>Gửi thông điệp</Button>
                </Col>
            </Row>
        </div>
    )
}

export default InputMessage;