import { Row, Col } from "reactstrap";

function TitleText() {
    return (
        <Row className="mt-5">
            <Col>
                <h1>Chào mừng đến với Devcamp 120</h1>
            </Col>
        </Row>
    )
}

export default TitleText;